class CreateTasks < ActiveRecord::Migration[5.0]
  def change
    create_table :tasks do |t|
      t.string :name
      t.references :project, foreign_key: true
      t.references :user, foreign_key: true
      t.text :description
      t.date :deadline
      t.boolean :done

      t.timestamps
    end

    add_index :tasks, [:project_id, :created_at]
  end
end
