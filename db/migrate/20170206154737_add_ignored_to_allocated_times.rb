class AddIgnoredToAllocatedTimes < ActiveRecord::Migration[5.0]
  def change
    add_column :allocated_times, :ignored, :boolean
  end
end
