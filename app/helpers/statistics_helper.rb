module StatisticsHelper

  def pie_chart_time_by_status
    Gchart.pie(title: 'Time by status',
      data: [@length_done, @length_ignored, @length_all - @length_ignored - @length_done],
      labels: ['done', 'ignored', 'future'],
      theme: :greyscale )
  end

  def pie_chart_tasks_by_status
    Gchart.pie(title: 'Tasks by status',
      data: [@number_of_tasks_done, @number_of_tasks_all - @number_of_tasks_done],
      labels: ['done', 'future'],
      theme: :greyscale)
  end
end
