class Task < ApplicationRecord
  belongs_to :project
  belongs_to :user
  has_many :allocated_times, dependent: :destroy
  default_scope -> { order(deadline: :asc) }
  scope :done, -> () { where(done: true) }
  validates :user_id, presence: true
  validates :project_id, presence: true
  validates :name, presence: true, length: { maximum: 100 }
  acts_as_taggable
end
