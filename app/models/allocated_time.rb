class AllocatedTime < ApplicationRecord
  belongs_to :task
  belongs_to :user
  default_scope -> { order(start: :asc) }
  #scope :done, -> () { where(done: true) }
  #scope :ignored, -> () { where(ignored: true) }
  validates :user_id, presence: true
  validates :task_id, presence: true
  validates :start, presence: true
  validates_datetime :end, after: :start, after_message: 'end must be after start'#, on_or_before: task.deadline, on_or_before_message: 'end must be on or before deadline'}

  def start_time
    self.start
  end

  def end_time
    self.end
  end

end
