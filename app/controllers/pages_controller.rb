class PagesController < ApplicationController
  def home
     if user_signed_in?
       redirect_to statistics_show_path
     end
  end
end
