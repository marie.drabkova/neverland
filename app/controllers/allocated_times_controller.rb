class AllocatedTimesController < ApplicationController
  load_and_authorize_resource
  before_action :set_atime, only: [:edit, :update, :destroy]
  before_action :set_task_project

  def new
    @allocated_time = AllocatedTime.new
  end

  def edit
  end

  def complete
    if @allocated_time.update_attributes(done: true, ignored: false)
      flash[:success] = 'Allocated time completed'
    end
    redirect_to @project
  end

  def ignore
    if @allocated_time.update_attributes(done: false, ignored: true)
      flash[:success] = 'Allocated time set as ignored'
    end
    redirect_to @project
  end

  def update
    if @allocated_time.update(allocated_time_params)
      flash[:success] = 'Time edited'
      redirect_to @project
    else
      render 'edit'
    end
  end

  def create
    @allocated_time = @task.allocated_times.create(allocated_time_params)
    if @allocated_time.done
      @allocated_time.ignored = false
    end
    @allocated_time.user_id = current_user.id
    if @allocated_time.save
      flash[:success] = "Time allocated"
      redirect_to @project
    else
      flash[:danger] = 'Time not allocated'
      render 'new'
    end
  end

  def destroy
    @allocated_time.destroy
    flash[:success] = "Time deleted"
    redirect_to @project
  end

  private
    def set_atime
      @allocated_time = AllocatedTime.find(params[:id])
    end

    def set_task_project
      @task = Task.find(params[:task_id])
      @project = Project.find_by(id: @task.project_id)
    end


    def allocated_time_params
      params.require(:allocated_time).permit(:project, :task, :user, :done, :start, :end)
    end
end
