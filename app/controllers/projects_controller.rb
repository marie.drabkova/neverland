class ProjectsController < ApplicationController
  load_and_authorize_resource
  before_action :set_project, only: [:destroy, :edit, :update, :show]

  def new
    @project = Project.new
  end

  def show
    @show_all = params[:show_all] #to pass to allocated_times
    if params[:tag] # filter by tag
      @tasks = @project.tasks.all.tagged_with(params[:tag]) # when filtering by tag, always show all
    elsif params[:show_all]
      @tasks = @project.tasks.all
    else
      @tasks = @project.tasks.where(done: [false, nil])
    end
    tasks_ids = @project.tasks.all.map { |t| t.id}
    taggings = ActsAsTaggableOn::Tagging.all.where(taggable_id: tasks_ids)
    taggings_ids = taggings.map { |t| t.tag_id }
    @tags = ActsAsTaggableOn::Tag.all.where(id: taggings_ids) # all tags associated with any task in given project
    render 'tasks/index'
  end

  def index
    redirect_to project_tasks_url(@projects.first, @projects.first.tasks) # show tasks of first project
  end

  def edit
  end

  def update
    if @project.update(project_params)
      flash[:success] = 'Project renamed'
      redirect_to @project
    else
      render 'edit'
    end
  end

  def create
    @project = current_user.projects.build(project_params)
    if @project.save
      flash[:success] = "Project created"
      redirect_to @project
    else
      render 'new'
    end
  end

  def destroy
    @project.destroy
    flash[:success] = "Project deleted"
    if @projects.any?
      redirect_to @projects.first
    else
      render 'pages/no_project'
    end
  end

  private
    def set_project
      @project = Project.find(params[:id])
    end

    def project_params
      params.require(:project).permit(:name)
    end
end
