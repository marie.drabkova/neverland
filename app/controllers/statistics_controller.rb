class StatisticsController < ApplicationController
  SECONDS_IN_HOUR = 3600

  def show
    # Projects
    @number_of_projects = @projects.count

    # Tasks
    @tasks = Task.all.where(user_id: current_user.id)
    @tasks_done = @tasks.where(done: true)
    @number_of_tasks_all = @tasks.count
    @number_of_tasks_done = @tasks_done.count

    # Allocated Times
    @allocated_times = AllocatedTime.all.where(user_id: current_user.id)
    @allocated_times_done = @allocated_times.where(done: true)
    @allocated_times_ignored = @allocated_times.where(ignored: true)

    @number_of_times_all = @allocated_times.count

    # lengths
    @lengths_all = @allocated_times.map { |at| at.end - at.start }
    if @lengths_all.length != 0
      @length_all = (@lengths_all.inject(:+)/SECONDS_IN_HOUR).round(1)
    else
      @length_all = 0
    end
    @lengths_done = @allocated_times_done.map { |at| at.end - at.start }
    if @lengths_done.length != 0
      @length_done = (@lengths_done.inject(:+)/SECONDS_IN_HOUR).round(1)
    else
      @length_done = 0
    end
    @lengths_ignored = @allocated_times_ignored.map { |at| at.end - at.start }
    if @lengths_ignored.length != 0
      @length_ignored = (@lengths_ignored.inject(:+)/SECONDS_IN_HOUR).round(1)
    else
      @length_ignored = 0
    end

    # average, min, max lengths
    if @number_of_times_all != 0
      @avg_length = (@length_all / @number_of_times_all).round(1)
    else
      @avg_length = 0
    end

    if @lengths_all.any?
      @max_length = (@lengths_all.max/SECONDS_IN_HOUR).round(1) || 0
      @min_length = (@lengths_all.min/SECONDS_IN_HOUR).round(1) || 0
    else
      @max_length = 0
      @min_length = 0
    end
  end


end
