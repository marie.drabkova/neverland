class NotesController < ApplicationController
  load_and_authorize_resource
  before_action :set_note, only: [:show, :edit, :update, :destroy]

  def index
    @notes = Note.all.where(user_id: current_user.id)
  end

  def new
    @note = Note.new
  end

  def edit
  end

  def create
    @note = Note.new(note_params)
    @note.user_id = current_user.id
    if @note.save
      flash[:success] = "Note created"
      redirect_to notes_path
    else
      render 'new'
    end
  end

  def update
    if @note.update(note_params)
      flash[:success] = 'Note edited'
      redirect_to notes_path
    else
      render 'edit'
    end
  end

  def destroy
    @note.destroy
    flash[:success] = "Note deleted"
    redirect_to notes_path
  end

  private
    def set_note
      @note = Note.find(params[:id])
    end

    def note_params
      params.require(:note).permit(:title, :content, :user_id)
    end
end
