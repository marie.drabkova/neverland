class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :set_projects

  private
    def set_projects # left menu
      if user_signed_in?
        @projects = current_user.projects
      else
        @projects = []
      end
    end
end
