class TasksController < ApplicationController
  load_and_authorize_resource
  before_action :set_task, only: [:edit, :update, :destroy, :complete]
  before_action :set_project

  def new
    @task = Task.new
  end

  def show
  end

  def edit
  end

  def complete
    if @task.update_attributes(done: true)
      flash[:success] = 'Task completed'
    end
    redirect_to @project
  end

  def uncomplete
    if @task.update_attributes(done: false)
      flash[:success] = 'Task completed'
    end
    redirect_to @project
  end

  def update
    if @task.update(task_params)
      flash[:success] = 'Task edited'
      redirect_to @project
    else
      render 'edit'
    end
  end

  def create
    @task = @project.tasks.create(task_params)
    @task.user_id = current_user.id
    if @task.save
      flash[:success] = "Task created"
      redirect_to @project
    else
      flash[:danger] = 'Task not created'
      render 'new'
    end
  end

  def destroy
    @task.destroy
    flash[:success] = "Task deleted"
    redirect_to @project
  end

  private
    def set_task
      @task = Task.find(params[:id])
    end

    def set_project
      @project = Project.find(params[:project_id])
    end

    def task_params
      params.require(:task).permit(:project, :user, :name, :description, :deadline, :show_all, :tag_list)
    end
end
