class CalendarController < ApplicationController

  def show
    @allocated_times = current_user.allocated_times.all
  end
end
