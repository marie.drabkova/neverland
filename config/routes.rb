Rails.application.routes.draw do
  resources :notes
  get 'statistics/show'
  get 'pages/no_project'
  get 'calendar/show'
  get 'pages/home'

  devise_for :users

  resources :projects do
    resources :tasks do
      resources :allocated_times do
        member do
          post :complete
          post :ignore
        end
      end

      member do
        post :complete
        post :uncomplete
      end
    end
  end

  root to: 'pages#home'
end
